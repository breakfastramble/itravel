﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Media.Imaging;
using ITravel.Utils;

namespace ITravel.Models
{
    public partial class Picture
    {
        [NotMapped]
        public BitmapImage ByteArrToBitmapImage
        {
            get
            {
                return GenericUtils.ByteArrayToImage(PictureSmall);
            }
        }
        public override string ToString() => $"Picture {Id}: {Description}";
    }
}
