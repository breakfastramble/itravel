﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel;
using System.Runtime.CompilerServices;
using System.ComponentModel.DataAnnotations;

namespace ITravel.Models
{
    public partial class Trip :  IValidatableObject
    {
        public override string ToString() => $"Trip {Id}: {Country}, {TripLocation},{DateStart:d}";

    public IEnumerable<ValidationResult> Validate(ValidationContext validationContext)
    { 
        List<ValidationResult>result = new List<ValidationResult>();
        if (Country.Length < 1 || Country.Length > 150)
        {
            result.Add(new ValidationResult("Country should be between 1 to 150 characters long")); //, new[] { "Country", "TripLocation"}
        }
        if (TripLocation.Length <1 || TripLocation.Length > 100)
        {
            result.Add(new ValidationResult("TripLocation should be between 1 to 100 characters long")); //, new[] { "Country", "TripLocation"}
        }
        return result;
    }


}
}
