﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Models;
using ITravel.Database;
using System.Data.Entity;

using System.Data.Entity.Validation;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for TripsDialog.xaml
    /// </summary>
    public partial class TripsDialog : Window
    {
        //*** Create storage for selected trip
        private Trip currentTrip;

        //*** Constructor
        public TripsDialog(Trip trip = null)
        {
            InitializeComponent();
            dpStartDate.SelectedDate = DateTime.Now;
            currentTrip = trip;
            if (trip != null)
            {
                tbCountry.Text = trip.Country;
                tbLocation.Text = trip.TripLocation;
                dpStartDate.SelectedDate = trip.DateStart;
                dpEndDate.SelectedDate = trip.DateEnd;
                tbComment.Text = trip.Comments;
                btAddUpdate.Content = "Update";
            }
        }

        //*** Add/Update trip
        private void btAddUpdateTrip_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trip tp = ReadUserInput();
                using (var ctx = new GenericDataRepository<Trip>())
                {
                    if (currentTrip == null)
                    {
                        ctx.Add(tp);
                        DialogResult = true;
                    }
                    else
                    {
                        currentTrip.Country = tp.Country;
                        currentTrip.TripLocation = tp.TripLocation;
                        currentTrip.DateStart = tp.DateStart;
                        currentTrip.DateEnd = tp.DateEnd;
                        currentTrip.Comments = tp.Comments;
                        ctx.Save(currentTrip);
                        DialogResult = true;
                    }
                }
            }
            catch (DbEntityValidationException ex)
            {
                string x = "";
                foreach (var error in ex.EntityValidationErrors)
                {
                    foreach (var validationError in error.ValidationErrors)
                    {
                        x += validationError.ErrorMessage + "\n";
                    }
                    MessageBox.Show(x, "Error Message", MessageBoxButton.OK, MessageBoxImage.Error);
                    error.Entry.State = EntityState.Detached;
                }

            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        //*** User input method for adding/updating trip
        private Trip ReadUserInput()
        {
            Trip tp = new Trip();
            tp.Country = tbCountry.Text;
            tp.TripLocation = tbLocation.Text;
            tp.DateStart = (DateTime)dpStartDate.SelectedDate;
            if (dpEndDate.SelectedDate == null)
            {
                tp.DateEnd = null;
            }
            else
            {
                tp.DateEnd = (DateTime)dpEndDate.SelectedDate;
            }
            if(tp.DateEnd < tp.DateStart)
            {
                MessageBox.Show("You are travelling back in Time.\nDo not step on butterflies!", "End Date warning message", MessageBoxButton.OK, MessageBoxImage.Information);
            }
            tp.Comments = tbComment.Text;
            return tp;
        }

    }
}
