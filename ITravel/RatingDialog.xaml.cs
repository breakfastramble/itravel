﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Models;
using ITravel.Database;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for RatingDialog.xaml
    /// </summary>
    public partial class RatingDialog : Window
    {
        //*** Create storage for selected trip
        private Trip CurrentTrip { get; set; }

        //*** Constructor
        public RatingDialog(Trip trip)
        {
            InitializeComponent();
            CurrentTrip = trip;
        }

        private void btDone_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (GenericDataRepository<Trip> ctx = new GenericDataRepository<Trip>()) // ex SystemException
                {
                    if (CurrentTrip == null) return; //should never happen
                    CurrentTrip.Rating = starRating.Value;
                    ctx.Save(CurrentTrip);
                    DialogResult = true;
                }
            }
            catch(SystemException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }
    }
}
