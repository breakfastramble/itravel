﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Models;
using ITravel.Database;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for DateEndDialog.xaml
    /// </summary>
    public partial class DateEndDialog : Window
    {
        Trip selectedTrip;
        public DateEndDialog(Trip tp = null)
        {
            InitializeComponent();
            selectedTrip = tp;
        }

        private void btAddDate_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using(GenericDataRepository<Trip> ctx = new GenericDataRepository<Trip>())
                {
                    selectedTrip.DateEnd = dpEndDate.SelectedDate;
                    selectedTrip.Status = true;
                    ctx.Save(selectedTrip);
                    DialogResult = true;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        private void btCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }
    }
}
