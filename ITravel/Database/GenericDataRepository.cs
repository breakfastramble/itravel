﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITravel.Models;

namespace ITravel.Database
{
    public class GenericDataRepository<T> : IDisposable, IGenericDataRepository<T> where T : class
    {
        private readonly DbSet<T> _table;
        private readonly TravelDbContext _db;
        protected TravelDbContext Context => _db;

        public GenericDataRepository()
        {
            _db = new TravelDbContext();
            _table = _db.Set<T>();
        }

        public void Dispose()
        {
            _db?.Dispose();
        }

        public int Add(T entity)
        {
            _table.Add(entity);
            return SaveChanges();
        }

        public int AddRange(IList<T> entities)
        {
            _table.AddRange(entities);
            return SaveChanges();
        }

        public int Save(T entity)
        {
            _db.Entry(entity).State = EntityState.Modified;
            return SaveChanges();
        }

        public int Delete(T entity)
        {
            _db.Entry(entity).State = EntityState.Deleted;
            return SaveChanges();
        }

        internal int SaveChanges()
        {
            return _db.SaveChanges();
        }

        public virtual List<T> ExecuteQuery(string sql) => _table.SqlQuery(sql).ToList();

        public virtual List<T> ExecuteQuery(string sql, object[] sqlParametersObjects) => _table.SqlQuery(sql, sqlParametersObjects).ToList();

        public virtual List<T> GetAll() => _table.ToList();

        public virtual T GetOne(int? id) => _table.Find(id);
    }
}
