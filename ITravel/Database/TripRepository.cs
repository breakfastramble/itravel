﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using ITravel.Models;

namespace ITravel.Database
{
    public class TripRepository : GenericDataRepository<Trip>
    {
        public override List<Trip> GetAll()
        {
            return Context.Trips.Where(b => b.Status == false).ToList();
        }
    }
}
