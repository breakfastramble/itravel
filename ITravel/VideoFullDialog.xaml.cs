﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Models;
using ITravel.Utils;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for VideoFullDialog.xaml
    /// </summary>
    public partial class VideoFullDialog : Window
    {
        //*** Create storage for selected Video
        private Video CurrentVideo { get; set; }
        public VideoFullDialog(Video video = null)
        {
            CurrentVideo = video;
            InitializeComponent();
            if (CurrentVideo != null)
            {
                InitialDataLoader();
            }
        }
        /*****  Method Initial data loader *****/
        private void InitialDataLoader()
        {
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    VideoFull currVideoFull = ctx.VideoFulls.Where(video => video.Id == CurrentVideo.Id).FirstOrDefault(); //ex: SystemException
                    
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
    }
}
