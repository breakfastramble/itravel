﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Models;
using ITravel.Database;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for BudgetDialog.xaml
    /// </summary>
    public partial class BudgetDialog : Window
    {
        //*** Create storage for selected trip
        private Trip CurrentTrip { get; set; }

        //*** Constructor
        public BudgetDialog(Trip trip = null)
        {
            InitializeComponent();
            CurrentTrip = trip;
            if(CurrentTrip.TripBudget != null) //Set values to the trip's current budget
            {
                tbTransport.Text = CurrentTrip.TripBudget.Transportation.ToString();
                tbAccommodation.Text = CurrentTrip.TripBudget.Accomodation.ToString();
                tbMeals.Text = CurrentTrip.TripBudget.Meals.ToString();
                tbEntertain.Text = CurrentTrip.TripBudget.Entertainment.ToString();
                btBudget.Content = "Update Budget";
                btDeleteBudget.Visibility = Visibility.Visible;
            }
            else
            {
                btDeleteBudget.Visibility = Visibility.Hidden;
            }
        }

        //*** Create Budget object
        public TripBudget Budget { get; set; }

        //*** Add/Update Budget for trip
        private void btAddUpdateBudget_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                using (GenericDataRepository<TripBudget> ctx = new GenericDataRepository<TripBudget>()) // ex SystemException
                {
                    if (CurrentTrip.TripBudget == null)
                    {
                        Budget = new TripBudget
                        {
                            Transportation = Convert.ToDecimal(tbTransport.Text), // ex FormatException and OverflowException
                            Accomodation = Convert.ToDecimal(tbAccommodation.Text), // ''
                            Meals = Convert.ToDecimal(tbMeals.Text), // ''
                            Entertainment = Convert.ToDecimal(tbEntertain.Text), // ''
                            Id = CurrentTrip.Id
                        };
                        ctx.Add(Budget);
                    } else
                    {
                        CurrentTrip.TripBudget.Transportation = Convert.ToDecimal(tbTransport.Text); // ex FormatException and OverflowException
                        CurrentTrip.TripBudget.Accomodation = Convert.ToDecimal(tbAccommodation.Text); // ''
                        CurrentTrip.TripBudget.Meals = Convert.ToDecimal(tbMeals.Text); // ''
                        CurrentTrip.TripBudget.Entertainment = Convert.ToDecimal(tbEntertain.Text); // ''
                        ctx.Save(CurrentTrip.TripBudget);
                    }
                    
                    DialogResult = true;
                }
                    
            }
            catch(Exception ex) when (ex is FormatException || ex is OverflowException || ex is SystemException)
            {
                MessageBox.Show("Error\n" + ex.Message, "Input Error", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
            
        }

        //*** Delete Budget from current trip
        private void btDeleteBudget_Click(object sender, RoutedEventArgs e)
        {
            // See if the user really wants to delete a Trip
            string msg = "Do you want to delete this trip budget?\n" + CurrentTrip.TripBudget.ToString();
            MessageBoxResult result = MessageBox.Show(msg, "Delete budget", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
            if (result == MessageBoxResult.OK)
            {
                try
                {
                    using (GenericDataRepository<TripBudget> ctx = new GenericDataRepository<TripBudget>())
                    {
                        ctx.Delete(CurrentTrip.TripBudget);
                        DialogResult = true;
                    }
                }
                catch (Exception ex) when (ex is FormatException || ex is OverflowException || ex is SystemException)
                {
                    MessageBox.Show("Error\n" + ex.Message);
                }
            }
              
        }
        //***Total Budget for Label (in window)
        public void BudgetTotal()
        {
            decimal transportation;
            decimal accommodation;
            decimal meals;
            decimal entertainment;
            try
            {
                if (tbTransport.Text == "")
                {
                    transportation = 0;
                }
                else
                {
                    transportation = Convert.ToDecimal(tbTransport.Text); // ex FormatException and OverflowException
                }
                if (tbAccommodation.Text == "")
                {
                    accommodation = 0;
                }
                else
                {
                    accommodation = Convert.ToDecimal(tbAccommodation.Text); // ''
                }
                if (tbMeals.Text == "")
                {
                    meals = 0;
                }
                else
                {
                    meals = Convert.ToDecimal(tbMeals.Text); // ''
                }
                if (tbEntertain.Text == "")
                {
                    entertainment = 0;
                }
                else
                {
                    entertainment = Convert.ToDecimal(tbEntertain.Text); // ''
                }

                lblTotal.Content = (transportation + accommodation + meals + entertainment).ToString();
            } catch(Exception ex) when (ex is FormatException || ex is OverflowException)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
            
        }

        //*** Input changed listeners (for calculating Total Budget label)
        private void tbTransport_TextChanged(object sender, TextChangedEventArgs e)
        {
            BudgetTotal();
        }

        private void tbAccommodation_TextChanged(object sender, TextChangedEventArgs e)
        {
            BudgetTotal();
        }

        private void tbMeals_TextChanged(object sender, TextChangedEventArgs e)
        {
            BudgetTotal();
        }

        private void tbEntertain_TextChanged(object sender, TextChangedEventArgs e)
        {
            BudgetTotal();
        }

    }
}
