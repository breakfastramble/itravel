﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Models;
using ITravel.Utils;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for PictureFullDialog.xaml
    /// </summary>
    public partial class PictureFullDialog : Window
    {
        //*** Create storage for selected Picture
        private Picture CurrentPicture { get; set; }
        public PictureFullDialog(Picture picture = null)
        {
            CurrentPicture = picture;
            InitializeComponent();
            if (CurrentPicture != null)
            {
                InitialDataLoader();
            }
        }
        /*****  Method Initial data loader *****/
        private void InitialDataLoader()
        {
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    PictureFull currPictureFull = ctx.PictureFulls.Where(picture => picture.Id == CurrentPicture.Id).FirstOrDefault(); //ex: SystemException
                    imageDlgFullSize.Source = GenericUtils.ByteArrayToImage(currPictureFull.PictureFullSize,1024);
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
    }
}
