﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using System.Xml;
using BingMapsRESTToolkit;
using Microsoft.Maps.MapControl.WPF;
using ITravel.Models;
using ITravel.Database;
using ITravel.Utils;
using System.IO;
using System.Net;
using System.Net.Http;


namespace ITravel
{
    /// <summary>
    /// Interaction logic for MapDialog.xaml
    /// </summary>
    public partial class MapDialog : Window
    {
        private Trip selectedTrip;
        private Microsoft.Maps.MapControl.WPF.Location selectedLocation;
        private Picture selectedImage = null;
        private const string Key = "AqNpULkX-l1BOAZCWvuekF-VTLyjwnQ0QhH9j4ZpW4RZTQ5drfa7mevujOQI1N_k"; // Bing map key
        private Microsoft.Maps.MapControl.WPF.Pushpin pin = new Microsoft.Maps.MapControl.WPF.Pushpin(); //map pushpin

        public MapDialog(Trip trip = null, Picture picture = null)
        {
            try
            {
                using (GenericDataRepository<Trip> ctx = new GenericDataRepository<Trip>())
                {
                    InitializeComponent();
                    //initial map setup
                    myMap.Focus();
                    myMap.Mode = new RoadMode();
                    myMap.SetView(center: new Microsoft.Maps.MapControl.WPF.Location(45.508888, -73.561668), 12);

                    // if picture selected
                    if (picture != null)
                    {
                        selectedImage = picture;
                        AddImageToMap();
                        return;
                    }

                    // if trip selected
                    if (trip != null)
                    {
                        // display country and location of selected trip
                        tbCountry.Text = trip.Country;
                        tbLocation.Text = trip.TripLocation;
                        // check if selected trip has latitude and longitude
                        if (trip.Lat != null && trip.lng != null)
                        {
                            // get location of the selected trip
                            selectedLocation = new Microsoft.Maps.MapControl.WPF.Location((double)trip.Lat, (double)trip.lng);
                        }
                        else
                        {
                            // find the location of the selected trip
                            selectedLocation = FindLocation();
                            // update latitude and longitude fields of the selected trip, save changes to DB
                            trip.Lat = (decimal?)selectedLocation.Latitude;
                            trip.lng = (decimal?)selectedLocation.Longitude;
                            ctx.Save(trip);
                        }
                        // center map on the selected location
                        myMap.SetView(center: selectedLocation, 10);
                        // display pushpin of the selected trip
                        selectedTrip = ctx.GetOne(trip.Id);
                        pin.Location = selectedLocation;
                        myMap.Children.Add(pin);
                        /*   //check if picture list is empty
                           if (selectedTrip.Pictures.Count == 0)
                           {
                               // if there no pictures, pushpin is displayed 
                               pin.Location = selectedLocation;
                               myMap.Children.Add(pin);
                           }
                           else
                           {
                               //if there pictures, the first small image is displayed
                               selectedImage = selectedTrip.Pictures.First<Picture>().PictureSmall;
                               AddImageToMap();
                           }*/
                    }
                }
            }
            catch (SystemException ex)
            {
                Console.WriteLine(ex.StackTrace);
                Environment.Exit(1);
            }
        }

        // add image to the map
        private void AddImageToMap()
        {
            try
            {
                if (selectedImage.Longitute != null && selectedImage.Latitude != null)
                {
                    MapLayer imageLayer = new MapLayer(); // new map layer
                    Image image = new Image();
                    image.Height = 150; // image height

                    // convert byte array to BitImage
                    byte[] img = selectedImage.PictureSmall;
                    BitmapImage myBitmapImage = Utils.GenericUtils.ByteArrayToImage(img, 150);
                    // image setup
                    image.Source = myBitmapImage;
                    image.Opacity = 1;
                    image.Stretch = System.Windows.Media.Stretch.None;

                    //Center the image around the location specified
                    PositionOrigin position = PositionOrigin.Center;
                    //Add the image to the defined map layer
                    selectedLocation = new Microsoft.Maps.MapControl.WPF.Location((double)selectedImage.Latitude, (double)selectedImage.Longitute);
                    myMap.SetView(center: selectedLocation, 15);
                    imageLayer.AddChild(image, selectedLocation, position);
                    //Add the image layer to the map
                    myMap.Children.Add(imageLayer);
                }
                else
                {
                    MessageBox.Show("Image has no coordinates", "Image Location Warning", MessageBoxButton.OK, MessageBoxImage.Warning);
                    //myMap.SetView(center: new Microsoft.Maps.MapControl.WPF.Location(45.508888, -73.561668), 12);
                }

            }
            catch (Exception ex) when (ex is SystemException || ex is IOException)
            {
                MessageBox.Show(ex.Message, "Error" + ex.Message, MessageBoxButton.OK, MessageBoxImage.Warning);
                Console.WriteLine(ex.StackTrace);
            }
        }



        // to show the search location on the map
        private void ShowLocationOnMap(object sender, RoutedEventArgs e)
        {
            // clean all pins from the amp
            myMap.Children.Remove(pin);
            // get coords of the search
            Microsoft.Maps.MapControl.WPF.Location loc = FindLocation();
            // set map center and zoom
            myMap.SetView(center: loc, zoomLevel: 10);
            // add a new pin of the searched location
            pin.Location = loc;
            myMap.Children.Add(pin);
        }

        // to find the location of the search
        private Microsoft.Maps.MapControl.WPF.Location FindLocation()
        {

            string country = tbCountry.Text;  // read user inputs
            string place = tbLocation.Text;  // read user inputs
            // if user input is empty set country to world, if location is empty, set default location
            if (country.Length == 0)
            {
                country = "world";
                if (place.Length == 0) place = "Montreal";
            }
            string geocodeRequest = $"http://dev.virtualearth.net/REST/v1/Locations?&countryRegion={country}&locality={place}&maxResults=1&key={Key}"; // a request BingMapAPI
            string response;
            Microsoft.Maps.MapControl.WPF.Location loc = new Microsoft.Maps.MapControl.WPF.Location();

            // get the requested information from virtualearth
            try
            {
                using (var client = new WebClient())
                {
                    response = client.DownloadString(geocodeRequest); // ArgumentNullException, WebException
                    Console.WriteLine(response); // to display the info on the console window for the dev
                }

                //to serilize the json data string
                System.Runtime.Serialization.Json.DataContractJsonSerializer ser = new System.Runtime.Serialization.Json.DataContractJsonSerializer(typeof(Response));
                using (var es = new MemoryStream(Encoding.Unicode.GetBytes(response)))
                {
                    //Response is one of the Bing Maps DataContracts
                    var mapResponse = (ser.ReadObject(es) as Response);
                    // get the first choice from the response ex. InvalidOperationException
                    BingMapsRESTToolkit.Location loctn = (BingMapsRESTToolkit.Location)mapResponse.ResourceSets.First().Resources.First(); 
                    // get longitude and latitude of the first location
                    double latitude = loctn.Point.Coordinates[0];
                    double longitude = loctn.Point.Coordinates[1];
                    //create wpf maps location
                    loc = new Microsoft.Maps.MapControl.WPF.Location(latitude, longitude);
                }
            }
            catch (Exception ex) when (ex is NotSupportedException || ex is ArgumentNullException || ex is InvalidOperationException)
            {
                MessageBox.Show("Location does not exist.\nReturn to default location", "Error", MessageBoxButton.OK, MessageBoxImage.Warning);
                tbCountry.Text = " ";
                tbLocation.Text = " ";
                Console.WriteLine(ex.StackTrace);
                return new Microsoft.Maps.MapControl.WPF.Location(45.508888, -73.561668);
            }
            return loc;
        }


    }
}
