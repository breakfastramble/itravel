﻿using System;
using System.Activities.Expressions;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITravel.Models
{
    public partial class TripBudget
    {
        [NotMapped]
        public double GetTotal => (double)(Transportation + Accomodation + Meals + Entertainment);
        
        public override string ToString() => $"Budget {Id}: Total: {GetTotal}";
        
    }
}
