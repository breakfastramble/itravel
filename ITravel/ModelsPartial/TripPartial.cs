﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITravel.Models
{
    public partial class Trip
    {
        public override string ToString() => $"Trip {Id}: {Country}, {TripLocation},{DateStart:d}";
        
    }
}
