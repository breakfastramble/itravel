﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ITravel.Models
{
public partial class Picture
    {
        public override string ToString() => $"Picture {Id}: {Description}";
        
    }
}
