﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.ComponentModel;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Database;
using ITravel.Models;
using MetadataExtractor;
using MetadataExtractor.Formats.Exif;
using Microsoft.Win32;
using SkiaSharp;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for ManagePictureDialog.xaml
    /// </summary>
    public partial class ManagePictureDialog : Window
    {
        //*** Create storage for selected trip
        private Trip CurrentTrip { get; set; }
        //TravelDbContext ctx;
        private decimal? lat = null;
        private decimal? lng = null;
        public ManagePictureDialog(Trip trip = null)
        {

            InitializeComponent();
            CurrentTrip = trip;
            if (CurrentTrip != null)
            {
                InitialDataLoader();
            }

        }

        /*****  Method Initial data loader *****/
        private void InitialDataLoader()
        {
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    ListBoxDlgGallery.ItemsSource = ctx.Pictures.Where(picture => picture.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
        /*****  Method to resize picture *****/
        private byte[] ResizePicture(string inputPath, int size, int quality)
        {
            using (var input = File.OpenRead(inputPath))
            {
                using (var inputStream = new SKManagedStream(input))
                {
                    using (var original = SKBitmap.Decode(inputStream))
                    {
                        int width, height;
                        if (original.Width > original.Height)
                        {
                            width = size;
                            height = original.Height * size / original.Width;
                        }
                        else
                        {
                            width = original.Width * size / original.Height;
                            height = size;
                        }
                        using (var resized = original.Resize(new SKImageInfo(width, height), SKBitmapResizeMethod.Lanczos3))
                        {
                            if (resized == null) return null;
                            using (var image = SKImage.FromBitmap(resized))
                            {
                                return image.Encode(SKEncodedImageFormat.Jpeg, quality).ToArray();
                            }
                        }
                    }
                }
            }

        }

        /*****  Method load pictures *****/
        private async Task<bool> LoadPictures()
        {
            bool done = false;
            await Task.Run(
                () =>
                {
                    // 1. Get selected photo(s)
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Filter = "Image files (*.jpg;*.jpeg;*.gif;*.png)|*.jpg;*.jpeg;*.gif;*.png";
                    dlg.Multiselect = true;
                    if (dlg.ShowDialog() == true)
                    {
                        try
                        {
                            using (TravelDbContext ctx = new TravelDbContext())
                            {
                                foreach (string file in dlg.FileNames)
                                {
                                    // read latitude and longitude from exif of image file
                                    var gps = ImageMetadataReader.ReadMetadata(dlg.FileName).OfType<GpsDirectory>()
                                     .FirstOrDefault();
                                    if (gps != null)
                                    {
                                        var location = gps.GetGeoLocation();
                                        lat = (decimal)location.Latitude;
                                        lng = (decimal)location.Longitude;
                                    }
                                    // 2. Create resized versions of the original image 150px and 1024px
                                    byte[] resizedImage150 = ResizePicture(file, 150, 100);
                                    byte[] resizedImage1024 = ResizePicture(file, 1024, 100);
                                    // 3. Create object picture
                                    Picture currPicture = new Picture { TripId = CurrentTrip.Id, PictureSmall = resizedImage150, Latitude = lat, Longitute = lng };
                                    // 4. Save new object to the Database
                                    ctx.Pictures.Add(currPicture);
                                    ctx.SaveChanges();
                                    // 5. Create object pictureFull
                                    PictureFull currPictureFull = new PictureFull { Id = currPicture.Id, PictureFullSize = resizedImage1024 };
                                    ctx.PictureFulls.Add(currPictureFull);
                                }
                                ctx.SaveChanges();
                                done = true;
                            }
                        }
                        catch (IOException ex)
                        {
                            MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        catch (SystemException ex)
                        {
                            MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                });
            return done;

        }
        /*****  Method delete pictures *****/
        private async Task<bool> DeletePictures()
        {
            List<Picture> selectedItems = ListBoxDlgGallery.SelectedItems.Cast<Picture>().ToList();
            bool done = false;
            await Task.Run(() =>
            {
                try
                {
                    using (TravelDbContext ctx = new TravelDbContext())
                    {
                        selectedItems.ForEach(picture =>
                        {
                            if (ctx.Entry(picture).State == System.Data.Entity.EntityState.Detached)
                            {
                                ctx.Pictures.Attach(picture);
                            }
                            ctx.Pictures.Remove(picture);
                        });
                        ctx.SaveChanges();
                    }
                    done = true;
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            });

            return done;
        }
        /*****  Listener Add pictures in the dialog *****/
        private async void btDlgAddImages_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTrip == null) return;
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    pbStatus.Visibility = Visibility.Visible;
                    bool done = await LoadPictures();
                    if (done)
                    {
                        ListBoxDlgGallery.ItemsSource = ctx.Pictures.Where(picture => picture.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                    }
                    pbStatus.Visibility = Visibility.Hidden;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        /*****  Listener Delete pictures from the list *****/
        private async void btDlgDeleteImages_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTrip == null) return;
            if (MessageBoxResult.Yes != MessageBox.Show(this, "Do you want to delete pictures?\n", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    pbStatus.Visibility = Visibility.Visible;
                    bool done = await DeletePictures();
                    if (done)
                    {
                        ListBoxDlgGallery.ItemsSource = ctx.Pictures.Where(picture => picture.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                    }
                    pbStatus.Visibility = Visibility.Hidden;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }
        /*****  Listener Double click on picture *****/
        private void ListBoxDlgGallery_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Picture selectedPicture = (Picture)ListBoxDlgGallery.SelectedItem;
            if (selectedPicture == null) return;
            try
            {

                PictureFullDialog dlg = new PictureFullDialog(selectedPicture);
                dlg.Owner = this;
                dlg.ShowDialog(); // ex InvalidOperationException
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        /*****  Listener Deselect all images when click on the empty area *****/
        private void ListBoxDlgGallery_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListBoxItem))
                ListBoxDlgGallery.UnselectAll();
        }
        /*****  Listener Update picture description *****/
        Picture currSelectedPicture; // Storage for selected picture from gallery
        private void btDlgUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (currSelectedPicture == null) return;
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    Picture pictureToUpdate = ctx.Pictures.Where(picture => picture.Id == currSelectedPicture.Id).FirstOrDefault(); //ex: SystemException
                    pictureToUpdate.Description = tbDlgPictureDescription.Text;
                    ctx.SaveChanges();
                    ListBoxDlgGallery.ItemsSource = ctx.Pictures.Where(picture => picture.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }

        }
        /*****  Listener Update picture description *****/
        private void ListBoxDlgGallery_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelectedPicture = (Picture)ListBoxDlgGallery.SelectedItem;
            if (currSelectedPicture != null)
            {
                btDlgUpdate.IsEnabled = true;
                tbDlgPictureDescription.IsEnabled = true;
                tbDlgPictureDescription.Text = currSelectedPicture.Description;

            }
            else
            {
                btDlgUpdate.IsEnabled = false;
                tbDlgPictureDescription.IsEnabled = false;
                tbDlgPictureDescription.Text = "";
            }

        }

        // display map dialog window
        private void btMap_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Picture selectedPicture = ListBoxDlgGallery.SelectedItem as Picture;
                MapDialog dlg = new MapDialog(picture: selectedPicture);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true) // ex InvalidOperationException
                {

                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }

        }
    }
}
