﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using ITravel.Models;
using ITravel.Database;
using System.ComponentModel;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        //*** Constructor
        public MainWindow()
        {
            InitializeComponent();
            RefreshList();
        }

        //*** Update ListView from database
        private void RefreshList()
        {
            try
            {
                using (TravelDbContext ctx = new TravelDbContext()) // ex SystemException
                {
                    lvTrips.ItemsSource = ctx.Trips.Include("TripBudget").ToList<Trip>(); //Explicit load for Trip budget
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        //*** Open Trips dialog window
        private void btAddUpdateTrip_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trip trip = (Trip)lvTrips.SelectedItem;
                TripsDialog dlg = new TripsDialog(trip);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true) // ex InvalidOperationException
                {
                    RefreshList();

                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        //*** Open Budget dialog
        private void btBudgetMain_Click(object sender, RoutedEventArgs e)
        {
            try
            {

                Trip trip = (Trip)lvTrips.SelectedItem;
                BudgetDialog dlg = new BudgetDialog(trip);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true) // ex InvalidOperationException
                {
                    RefreshList();
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }


        }

        /*****  Listener Picture Gallery Button *****/
        private void btPicture_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trip trip = (Trip)lvTrips.SelectedItem;
                ManagePictureDialog dlg = new ManagePictureDialog(trip);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true) // ex InvalidOperationException
                {
                    RefreshList();
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }
        //*** View Video Gallery window
        private void btVideo_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trip trip = (Trip)lvTrips.SelectedItem;
                ManageVideoDialog dlg = new ManageVideoDialog(trip);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true) // ex InvalidOperationException
                {
                    RefreshList();
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        //*** View Map window
        private void btMap_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trip selectedTrip = lvTrips.SelectedItem as Trip;
                MapDialog dlg = new MapDialog(selectedTrip);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true) // ex InvalidOperationException
                {
                    RefreshList();
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        //*** Listview Item selected
        private void lvTrips_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            Trip trip = lvTrips.SelectedItem as Trip;
            
            btAddUpdate.Content = "Add a trip";
            if (trip !=null)
            {
                btPicture.Foreground = Brushes.AntiqueWhite;
                btBudget.Foreground = Brushes.AntiqueWhite;
                btVideo.Foreground = Brushes.AntiqueWhite;
                btAddUpdate.Content = "Update trip";
            }
            if (trip?.TripBudget == null)
            {
                btBudget.Content = "Add Budget";
            }
            else
            {
                btBudget.Content = "Update Budget";
            }
        }

        //*** Open Rating window
        private void btStarRating_Click(object sender, RoutedEventArgs e)
        {
            try
            {
                Trip selectedTrip = lvTrips.SelectedItem as Trip;
                RatingDialog dlg = new RatingDialog(selectedTrip);
                dlg.Owner = this;
                if (dlg.ShowDialog() == true)
                {
                    RefreshList();
                }
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }

        //*** Window Closing
        private void Window_Closing(object sender, System.ComponentModel.CancelEventArgs e)
        {
            // See if the user really wants to shut down this window.
            string msg = "Do you want to leave?";
            MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.YesNo, MessageBoxImage.Warning);

            if (result == MessageBoxResult.No)
            {
                // If user doesn't want to close, cancel closure.
                e.Cancel = true;
            }
        }

        //*** Exit menu item
        private void miExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        //*** Delete Trip
        private void miDeleteTrip_Click(object sender, RoutedEventArgs e)
        {
            Trip trip = lvTrips.SelectedItem as Trip;
            
            if (trip == null) // Dialog to aks user choose the trip to delete
            {
                string msg = "Select trip to delete\n";
                MessageBoxResult result = MessageBox.Show(msg, "Trip not selected", MessageBoxButton.OK, MessageBoxImage.Warning);
                return;
            }
            if (trip != null)
            {
                // See if the user really wants to delete a Trip
                string msg = "Do you want to delete the record?\n" + trip.ToString();
                MessageBoxResult result = MessageBox.Show(msg, "My App", MessageBoxButton.OKCancel, MessageBoxImage.Warning);
                if (result == MessageBoxResult.OK)
                {
                    try
                    {   // tried to figure out cascade delete, but failed!
                        using (var ctx = new GenericDataRepository<TripBudget>()) // ex SystemException
                        {
                            var budget = trip.TripBudget;
                            if (budget != null)
                            {
                                ctx.Delete(budget);
                            }
                        }
                        using (var ctx = new GenericDataRepository<Trip>()) // ex SystemException
                        {

                            ctx.Delete(trip);
                            RefreshList();
                        }
                    }
                    catch (SystemException ex)
                    {
                        MessageBox.Show("Error\n" + ex.Message);
                    }
                }
            }
        }


        //*** Sort by Country
        private void miSortByCountry_Click(object sender, RoutedEventArgs e)
        {
            lvTrips.Items.SortDescriptions.Clear();
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTrips.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("Country", ListSortDirection.Ascending));
        }

        //*** Sort by Location
        private void miSortByLocation_Click(object sender, RoutedEventArgs e)
        {
            lvTrips.Items.SortDescriptions.Clear();
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTrips.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("TripLocation", ListSortDirection.Ascending));
        }

        //*** Sort by Start Date
        private void miSortByStartDate_Click(object sender, RoutedEventArgs e)
        {
            lvTrips.Items.SortDescriptions.Clear();
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTrips.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("DateStart", ListSortDirection.Ascending));
        }

        //*** Sort by End Date
        private void miSortByEndDate_Click(object sender, RoutedEventArgs e)
        {
            lvTrips.Items.SortDescriptions.Clear();
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTrips.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("DateEnd", ListSortDirection.Ascending));
        }

        //*** Sort by Rating
        private void miSortByRating_Click(object sender, RoutedEventArgs e)
        {
            lvTrips.Items.SortDescriptions.Clear();
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTrips.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("Rating", ListSortDirection.Ascending));
        }

        //*** Sort by Total Cost
        private void miSortByTotalCost_Click(object sender, RoutedEventArgs e)
        {
            lvTrips.Items.SortDescriptions.Clear();
            CollectionView view = (CollectionView)CollectionViewSource.GetDefaultView(lvTrips.ItemsSource);
            view.SortDescriptions.Add(new SortDescription("TripBudget.GetTotal", ListSortDirection.Ascending));
        }
    }
}
