﻿using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;
using ITravel.Models;
using Microsoft.Win32;

namespace ITravel
{
    /// <summary>
    /// Interaction logic for ManageVideoDialog.xaml
    /// </summary>
    public partial class ManageVideoDialog : Window
    {
        //*** Create storage for selected trip
        private Trip CurrentTrip { get; set; }
        public ManageVideoDialog(Trip trip = null)
        {
            InitializeComponent();
            CurrentTrip = trip;
            if (CurrentTrip != null)
            {
                InitialDataLoader();
            }
        }
        /*****  Method Initial data loader *****/
        private void InitialDataLoader()
        {
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    ListBoxDlgGallery.ItemsSource = ctx.Videos.Where(video => video.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
        /*****  Method create Thumbnail Frame *****/
        private byte[] CreateThumbNailFrame(string videoPath)
        {
            byte[] CurrThumbnailFromVideo;
            MediaPlayer mediaPlayer = new MediaPlayer();
            mediaPlayer.ScrubbingEnabled = true;
            mediaPlayer.Position = TimeSpan.FromSeconds(0);
            mediaPlayer.Open(new Uri(videoPath));
            System.Threading.Thread.Sleep(2000); // TODO:Modify method. Need listener to load player 
            DrawingVisual drawingVisual = new DrawingVisual();
            DrawingContext drawingContext = drawingVisual.RenderOpen();
            drawingContext.DrawVideo(mediaPlayer, new Rect(0, 0, 160, 100));
            drawingContext.Close();
            double dpiX = 1 / 200;
            double dpiY = 1 / 200;
            RenderTargetBitmap bmp = new RenderTargetBitmap(160, 100, dpiX, dpiY, PixelFormats.Pbgra32);
            bmp.Render(drawingVisual);
            //</ draw video_image >
            var encoder = new JpegBitmapEncoder();
            encoder.Frames.Add(BitmapFrame.Create(bmp));
            using (var stream = new MemoryStream())
            {

                encoder.Save(stream);
                CurrThumbnailFromVideo = stream.ToArray();
            }
            mediaPlayer.Close();
            return CurrThumbnailFromVideo;
        }
        /*****  Method load video *****/
        private async Task<bool> LoadVideos()
        {
            bool done = false;
            await Task.Run(
                () =>
                {
                    // 1. Get selected video
                    OpenFileDialog dlg = new OpenFileDialog();
                    dlg.Filter = "Video files (*.avi;*.mov;*.mp4;*.ogg;*.wmv;*.webm)|*.avi;*.mov;*.mp4;*.ogg;*.wmv;*.webm";
                    if (dlg.ShowDialog() == true)
                    {
                        try
                        {
                            using (TravelDbContext ctx = new TravelDbContext())
                            {
                                // 2. Read Video from file
                                byte[] currVideoFile = File.ReadAllBytes(dlg.FileName);
                                // 3. Create ThumbNail from video
                                byte[] CurrThumbnailFromVideo = CreateThumbNailFrame(dlg.FileName);
                                // 4. Create object Video
                                Video currVideo = new Video { TripId = CurrentTrip.Id, VideoSnapshot = CurrThumbnailFromVideo };
                                // 5. Save new object to the Database
                                ctx.Videos.Add(currVideo);
                                ctx.SaveChanges();
                                // 6. Create object pictureFull
                                VideoFull currVideoFull = new VideoFull { Id = currVideo.Id, Video = currVideoFile };
                                ctx.VideoFulls.Add(currVideoFull);
                                // 5. Save new object to the Database
                                ctx.SaveChanges();
                                done = true;
                            }
                        }
                        catch (IOException ex)
                        {
                            MessageBox.Show(ex.Message, "File reading failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                        catch (SystemException ex)
                        {
                            MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                        }
                    }
                });
            return done;
        }

        /*****  Method delete videos *****/
        private async Task<bool> DeleteVideos()
        {
            List<Video> selectedItems = ListBoxDlgGallery.SelectedItems.Cast<Video>().ToList();
            bool done = false;
            await Task.Run(() =>
            {
                try
                {
                    using (TravelDbContext ctx = new TravelDbContext())
                    {
                        selectedItems.ForEach(video =>
                        {
                            if (ctx.Entry(video).State == System.Data.Entity.EntityState.Detached)
                            {
                                ctx.Videos.Attach(video);
                            }
                            ctx.Videos.Remove(video);
                        });
                        ctx.SaveChanges();
                    }
                    done = true;
                }
                catch (SystemException ex)
                {
                    MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
                }
            });

            return done;
        }
        /*****  Listener Add videos in the dialog *****/
        private async void btDlgAddVideo_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTrip == null) return;
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    pbStatus.Visibility = Visibility.Visible;
                    bool done = await LoadVideos();
                    if (done)
                    {
                        ListBoxDlgGallery.ItemsSource = ctx.Videos.Where(video => video.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                    }
                    pbStatus.Visibility = Visibility.Hidden;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }

        }

        /*****  Listener Delete pictures from the list *****/
        private async void btDlgDeleteVideo_Click(object sender, RoutedEventArgs e)
        {
            if (CurrentTrip == null) return;
            if (MessageBoxResult.Yes != MessageBox.Show(this, "Do you want to delete videos?\n", "Warning", MessageBoxButton.YesNo, MessageBoxImage.Warning))
            { return; }
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    pbStatus.Visibility = Visibility.Visible;
                    bool done = await DeleteVideos();
                    if (done)
                    {
                        ListBoxDlgGallery.ItemsSource = ctx.Videos.Where(video => video.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                    }
                    pbStatus.Visibility = Visibility.Hidden;
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Warning);
            }
        }
        /*****  Listener Update picture description *****/
        Video currSelectedVideo; // Storage for selected video from gallery
        private void btDlgUpdate_Click(object sender, RoutedEventArgs e)
        {
            if (currSelectedVideo == null) return;
            try
            {
                using (TravelDbContext ctx = new TravelDbContext())
                {
                    Video videoToUpdate = ctx.Videos.Where(video => video.Id == currSelectedVideo.Id).FirstOrDefault(); //ex: SystemException
                    videoToUpdate.Description = tbDlgVideoDescription.Text;
                    ctx.SaveChanges();
                    ListBoxDlgGallery.ItemsSource = ctx.Videos.Where(video => video.TripId == CurrentTrip.Id).ToList(); //ex: SystemException
                }
            }
            catch (SystemException ex)
            {
                MessageBox.Show(ex.Message, "Database operation failed", MessageBoxButton.OK, MessageBoxImage.Error);
                Environment.Exit(1); // fatal error
            }
        }
        
        /*****  Listener Double click on video *****/
        /* TODO: Play video from separate window
        private void ListBoxDlgGallery_MouseDoubleClick(object sender, MouseButtonEventArgs e)
        {
            Video selectedVideo = (Video)ListBoxDlgGallery.SelectedItem;
            if (selectedVideo == null) return;
            try
            {
                VideoFullDialog dlg = new VideoFullDialog(selectedVideo);
                dlg.Owner = this;
                dlg.ShowDialog(); // ex InvalidOperationException
            }
            catch (InvalidOperationException ex)
            {
                MessageBox.Show("Error\n" + ex.Message);
            }
        }
        */
        /*****  Listener Deselect all images when click on the empty area *****/
        private void ListBoxDlgGallery_MouseDown(object sender, MouseButtonEventArgs e)
        {
            HitTestResult r = VisualTreeHelper.HitTest(this, e.GetPosition(this));
            if (r.VisualHit.GetType() != typeof(ListBoxItem))
                ListBoxDlgGallery.UnselectAll();
        }
        /*****  Listener Update video description *****/
        private void ListBoxDlgGallery_SelectionChanged(object sender, SelectionChangedEventArgs e)
        {
            currSelectedVideo = (Video)ListBoxDlgGallery.SelectedItem;
            if (currSelectedVideo != null)
            {
                btDlgUpdate.IsEnabled = true;
                tbDlgVideoDescription.IsEnabled = true;
                tbDlgVideoDescription.Text = currSelectedVideo.Description;
            }
            else
            {
                btDlgUpdate.IsEnabled = false;
                tbDlgVideoDescription.IsEnabled = false;
                tbDlgVideoDescription.Text = "";
            }
        }
    }
}
