﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;

namespace ITravel.Utils
{
    public class GenericUtils
    {

        public static BitmapImage ByteArrayToImage(byte[] byteArrayIn, int imageWidth = 150)
        {
            using (MemoryStream ms = new MemoryStream(byteArrayIn))
            {
                BitmapImage bmp = new BitmapImage();
                bmp.BeginInit();
                bmp.CacheOption = BitmapCacheOption.OnLoad;
                bmp.DecodePixelWidth = imageWidth;
                bmp.StreamSource = ms;
                bmp.EndInit();
                return bmp;
            }
        }

     }
}
