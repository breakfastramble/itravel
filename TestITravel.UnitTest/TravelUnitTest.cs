﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using ITravel.Models;
using ITravel.Utils;
using ITravel;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using ITravel.Database;

namespace TestITravel.UnitTest
{
    [TestClass]
    public class TravelUnitTest
    {
        [TestMethod]
        public void ToStringTest_Picture_ReturnsString()
        {
            //Arrange
            Picture picture = new Picture
            {
                Id = 1,
                PictureSmall = null,
                Description = "Nice"
            };
            // Act
            var result = picture.ToString();
            // Assert
            Assert.AreEqual("Picture 1: Nice", result);
        }

        [TestMethod]
        public void ToStringTest_Trip_ReturnsString()
        {
            //Arrange
            Trip trip = new Trip
            {
                Id = 1,
                Country = "Canada",
                TripLocation = "Montreal",
                DateStart = DateTime.Now
            };
            // Act
            var result = trip.ToString();
            // Assert
            Assert.AreEqual("Trip 1: Canada, Montreal,2021-04-18", result);
        }

        [TestMethod]
        public void ToStringTest_BudgetGetTotal_ReturnsString()
        {
            //Arrange
            TripBudget budget = new TripBudget
            {
                Id = 1,
                Transportation = 200,
                Accomodation = 300,
                Meals = 150,
                Entertainment = 75
            };
            //Act
            var result = budget.ToString();
            //Assert
            Assert.AreEqual("Budget 1: Total: 725", result);
        }

        [TestMethod]
        public void ValidateTest_TripCountryEmprty_ReturnErrorMessage()
        {
            //Arrange
            Trip trip = new Trip
            {
                Id = 1,
                Country = "",
                TripLocation = "Montreal",
                DateStart = DateTime.Now
            };
            var ctx = new ValidationContext(trip, null, null);
            // Act
            List<ValidationResult> listResult = (List<ValidationResult>)trip.Validate(ctx);
            var result = listResult.ToArray();
            // Assert
            Assert.AreEqual("Country should be between 1 to 150 characters long", result[0].ToString());
        }

        [TestMethod]
        public void ValidateTest_TripLocationEmpty_ReturnErrorMessage()
        {
            //Arrange
            Trip trip = new Trip
            {
                Id = 1,
                Country = "Canada",
                TripLocation = "",
                DateStart = DateTime.Now
            };
            var ctx = new ValidationContext(trip, null, null);
            // Act
            List<ValidationResult> listResult = (List<ValidationResult>)trip.Validate(ctx);
            var result = listResult.ToArray();
            // Assert
            Assert.AreEqual("TripLocation should be between 1 to 100 characters long", result[0].ToString());
        }

        [TestMethod]
        public void GetOneTest_Trip_ReturnTripCountry()
        {
            Trip result = new Trip();
            using (GenericDataRepository<Trip> ctx = new GenericDataRepository<Trip>())
            {
                // Arrange
                int id = 1;
                // Act
                result = ctx.GetOne(id);
            }
            // Assert
            Assert.AreEqual("England", result.Country);
        }

        [TestMethod]
        public void GetAllTest_TripList_ReturnFirstTripCountry()
        {
            using (GenericDataRepository<Trip> ctx = new GenericDataRepository<Trip>())
            {
                // Arrange

                // Act
                var listResult = ctx.GetAll();
                var result = listResult.ToArray();
                // Assert
                Assert.AreEqual("England", result[0].Country);
            }
        }
        [TestMethod]
        public void 

    }
}
